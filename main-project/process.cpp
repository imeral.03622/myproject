#include "phone_conversations.h"

double total_duraction(phone_conversations* array[], int size) {
	double ttl = 0;
	for (int i = 1; i < size; i++) {
		ttl = ttl + array[i]->duraction.hours * 3600 + array[i]->duraction.minutes * 60 + array[i]->duraction.seconds;
	}
	return ttl;
}

double total_cost(phone_conversations* array[], int size) {
	double ttl = 0;
	for (int i = 1; i < size; i++) {
		ttl = ttl + array[i]->cost;
	}
	return ttl;
}

double process(phone_conversations* array[], int size) {
	
	//

	double avg = total_cost(array, size) / total_duraction(array, size);

	//
	
	return avg;
}
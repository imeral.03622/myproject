#ifndef PHONE_CONVERSATIONS_H
#define PHONE_CONVERSATIONS_H

#include "constants.h"

struct Date
{
    int day;
    int month;
    int year;
};

struct Time
{
    int hours;
    int minutes;
    int seconds;
};

struct phone_conversations
{
    char phone_number[MAX_STRING_SIZE]; 
    Date date;
    Time time;
    Time duraction;
    char tarif[MAX_STRING_SIZE];
    double cost;
};

#endif
#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

Date convert_date(char* str)
{
    Date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.year = atoi(str_number);
    return result;
}

Time convert_time(char* str)
{
    Time result;
    char* context = NULL;
    char* str_number = strtok_s(str, ":", &context);
    result.hours = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.minutes = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.seconds = atoi(str_number);
    return result;
}

void read(const char* file_name, phone_conversations* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            phone_conversations* item = new phone_conversations;
            file >> item->phone_number;
            file >> tmp_buffer;
            item->date = convert_date(tmp_buffer);
            file >> tmp_buffer;
            item->time = convert_time(tmp_buffer);
            file >> tmp_buffer;
            item->duraction = convert_time(tmp_buffer);
            file >> item->tarif;
            file >> item->cost;
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}

//////////////////////

/* 

<data_structure>** <function_name> (
    <data_structure>* array[],
    int size,
    bool (*check)(<data_structure>* element),
    int& result_size
);

/*
�������� ������� <function_name>:
    ������� ���������� ������ � ��������� ������� � ��� ��������� �� ��������,
    ��� ������� ������� ������ ���������� �������� true, ���������� � �����
    ������, ��������� �� ������� ������������ ��������

���������:
    array       - ������ � ��������� �������
    size        - ������ ������� � ��������� �������
    check       - ��������� �� ������� ������.
                  � �������� �������� ����� ��������� ����� �������� ���
                  ������� ������, �������� ������� ������� ����
    result_data - ��������, ������������ �� ������ - ����������, � �������
                  ������� ������� ������ ��������������� �������

������������ ��������
    ��������� �� ������ �� ���������� �� ��������, ��������������� �������
    ������ (��� ������� ������� ������ ���������� true)



bool <check_function_name> (
    <data_structure>* element
    );

/*
�������� ������� <check_function_name>:
    ������� ������ - ���������, ������������� �� ���� ������� ������� ������

���������:
    element - ��������� �� �������, ������� ����� ���������

������������ ��������
    true, ���� ������� ������������� ������� ������, � false � ���� ������
*/
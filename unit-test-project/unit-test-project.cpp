#include "pch.h"
#include "CppUnitTest.h"
#include "../main-project/phone_conversations.h"
#include "../main-project/processing.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestproject
{
phone_conversations* build_conversations(int duraction_hours, int duraction_minutes, int duraction_seconds, float cost)
{
	phone_conversations* conversations = new phone_conversations;
	conversations->duraction.hours = duraction_hours;
	conversations->duraction.minutes = duraction_minutes;
	conversations->duraction.seconds = duraction_seconds;
	conversations->cost = cost;
	return conversations;
}

//       
void delete_conversations(phone_conversations* array[], int size)
{
	for (int i = 0; i < size; i++)
	{
		delete array[i];
	}
}

TEST_CLASS(unittestproject)
{
public:
	TEST_METHOD(TestMethod1) //     
	{
		phone_conversations* conversationss[3];
		conversationss[0] = build_conversations(0, 1, 2, 0.1); 
		conversationss[1] = build_conversations(0, 1, 2, 0.2); 
		conversationss[2] = build_conversations(0, 1, 2, 0.3); 
		Assert::AreEqual(0.00403226, process(conversationss, 3));
		delete_conversations(conversationss, 3);
	}

	TEST_METHOD(TestMethod2) //        
	{
		phone_conversations* conversationss[3];
		conversationss[0] = build_conversations(0, 0, 2, 0.1); 
		conversationss[1] = build_conversations(0, 0, 2, 0.2); 
		conversationss[2] = build_conversations(0, 0, 2, 0.3); 
		Assert::AreEqual(0.125, process(conversationss, 3));
		delete_conversations(conversationss, 3);
	}

	TEST_METHOD(TestMethod3) //         
	{
		phone_conversations* conversationss[3];
		conversationss[0] = build_conversations(0, 10, 2, 0.1);
		conversationss[1] = build_conversations(0, 10, 2, 0.2);
		conversationss[2] = build_conversations(0, 10, 2, 0.3);
		Assert::AreEqual(0.5, process(conversationss, 3));
		delete_conversations(conversationss, 3);
	}

	TEST_METHOD(TestMethod4) //         
	{
		phone_conversations* conversationss[3];
		conversationss[0] = build_conversations(0, 0, 1, 0.1);
		conversationss[1] = build_conversations(0, 0, 1, 0.2); 
		conversationss[2] = build_conversations(0, 0, 1, 0.3); 
		Assert::AreEqual(0.25, process(conversationss, 3));
		delete_conversations(conversationss, 3);
	}

	TEST_METHOD(TestMethod5) //          
	{
		phone_conversations* conversationss[3];
		conversationss[0] = build_conversations(0, 1, 2, 0.1); 
		conversationss[1] = build_conversations(0, 2, 3, 0.2); 
		conversationss[2] = build_conversations(0, 3, 4, 0.3); 
		Assert::AreEqual(0.00162866, process(conversationss, 3));
		delete_conversations(conversationss, 3);
	}

	TEST_METHOD(TestMethod6) //          
	{
		phone_conversations* conversationss[3];
		conversationss[0] = build_conversations(0, 1, 2, 0.2);
		conversationss[1] = build_conversations(0, 1, 2, 0.2); 
		conversationss[2] = build_conversations(0, 1, 2, 0.2); 
		Assert::AreEqual(0.00322581, process(conversationss, 3));
		delete_conversations(conversationss, 3);
	}

	TEST_METHOD(TestMethod7) //     
	{
		phone_conversations* conversationss[3];
		conversationss[0] = build_conversations(0, 1, 2, 0.2); 
		conversationss[1] = build_conversations(0, 1, 2, 0.4); 
		conversationss[2] = build_conversations(0, 1, 2, 0.6); 
		Assert::AreEqual(0.00806452, process(conversationss, 3));
		delete_conversations(conversationss, 3);
	}

	TEST_METHOD(TestMethod8) //    
	{
		phone_conversations* conversationss[3];
		conversationss[0] = build_conversations(0, 3, 1, 0.3); 
		conversationss[1] = build_conversations(0, 2, 2, 0.6); 
		conversationss[2] = build_conversations(0, 1, 3, 0.9); 
		Assert::AreEqual(0.00810811, process(conversationss, 3));
		delete_conversations(conversationss, 3);
	}
};
}
